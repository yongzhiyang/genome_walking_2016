"""
sort final homologs from .subtrees, merged clusters and swipe to beta

swipe output colums are:
Query id, Subject id, % identity, alignment length, mismatches, 
gap openings, q. start, q. end, s. start, s. end, e-value, bit score

Output fasta files of homologs adding transcripts
"""

import sys,os
import seq
import tree_utils
import phylo3,newick3
from Bio import SeqIO

def get_beta_geneid(longid):
	"""input longid looks like Beta@Bv3_054900_nows_t1
	or Bv3_054900_nows.raxml.tre.tt.mm
	or Bv1_000120_fdjg_1.subtree
	extract the unique beta gene id Bv3_054900_nows"""
	if "@" in longid: # for seqid
		longid = longid.split("@")[1]
	longid = longid.split(".")[0] # for file names
	# _t1, _t2... all are the same gene
	spls = (longid.split(".")[0]).split("_")
	if len(spls) >= 3:
		return spls[0]+"_"+spls[1]+"_"+spls[2]
	else: return None
	
def write_dict(DICT,outname):
	"""for dict having a list as the value"""
	outfile = open(outname,"w")
	for key in DICT:
		outfile.write(key+":")
		for i in DICT[key]:
			outfile.write(i+"\t")
		outfile.write("\n")
	outfile.close()
	return

def read_dict(inname):
	"""construct a dict having a list as the value"""
	DICT = {}
	infile = open(inname,"r")
	for line in infile:
		spls = line.strip().split(":")
		if len(spls) != 2: continue
		key,values = spls[0],spls[1]
		for value in values.split("\t"):
			if key not in DICT:
				DICT[key] = []
			DICT[key].append(value)
	infile.close()
	return DICT


if __name__ == "__main__":
	if len(sys.argv) != 6:
		print "python "+sys.argv[0]+" .subtreeDIR beta_merged swipe_to_beta_DIR all_fasta outdir"
		sys.exit(0)
	
	subtreeDIR, beta_merged,swipe_to_beta_DIR,all_fasta,outdir = sys.argv[1:]
	
	# parse all the transcriptome to beta swipe output
	# SWIPE output line looks like:
	# AAXJ@0	lcl|Beta@Bv_006080_hoye_t1	79.25	241	49	1	1	241	9	248	4.9e-112	399.8
	transDICT = {} # key is beta geneid, value is a list of transcriptome seqid
	for swipe_out in os.listdir(swipe_to_beta_DIR):
		if not swipe_out.endswith(".swipe"): continue
		print swipe_out
		infile = open(swipe_to_beta_DIR+"/"+swipe_out,"r")
		seqDICT = {} # key is transcriptome seqid, value is beta geneid
		for line in infile:
			if len(line) < 3: continue
			spls = line.strip().split("\t")
			query = spls[0]
			hit = get_beta_geneid(spls[1])
			if query not in seqDICT:
				seqDICT[query] = hit
				if hit not in transDICT:
					transDICT[hit] = []
				transDICT[hit].append(query)
		infile.close()
	write_dict(transDICT,"transDICT")
	print "No. beta geneid:",len(transDICT)
	
	genomeDICT = {} # key is beta geneid, value is a list of genome seqid
	# parse the subtreeDIR and add genome seqs to each corresponding beta_geneid
	for subtree in os.listdir(subtreeDIR):
		if not subtree.endswith(".subtree"): continue
		beta_geneid = get_beta_geneid(subtree)
		print subtree,beta_geneid
		with open(subtreeDIR+"/"+subtree,"r") as infile:
			intree = newick3.parse(infile.readline())
		if beta_geneid not in genomeDICT:
			genomeDICT[beta_geneid] = []
		genomeDICT[beta_geneid] += tree_utils.get_front_labels(intree)
	write_dict(genomeDICT,"genomeDICT")
			
	# write the merged clusters
	count = 0
	transDICT = read_dict("transDICT")
	genomeDICT = read_dict("genomeDICT")
	infile = open(beta_merged,"r")
	homologDICT = {} # key is homolog id, value is a set of genome and trans seqids
	for line in infile:
		spls = line.strip().split("\t")
		if len(spls) == 0: continue
		count += 1
		homologid = "homolog"+str(count)
		homologDICT[homologid] = set()
		for beta_seqid in spls:
			homologDICT[homologid].add(beta_seqid)
			beta_geneid = get_beta_geneid(beta_seqid)
			if beta_geneid in transDICT:
				for i in transDICT[beta_geneid]:
					homologDICT[homologid].add(i)
			if beta_geneid in genomeDICT:
				for i in genomeDICT[beta_geneid]:
					homologDICT[homologid].add(i)
	infile.close()
	write_dict(homologDICT,"homologDICT")

	homoDICT = {} # key is seqid, value is a list of homolog ids
	with open("homologDICT","r") as infile:
		for line in infile:
			spls = line.strip().split(":")
			if len(spls) != 2: continue
			for seqid in spls[1].split("\t"):
				if seqid not in homoDICT:
					homoDICT[seqid] = []
				homoDICT[seqid].append(spls[0])
	handle = open(all_fasta,"r")
	outfile1 = open("leftovers","w")
	for s in SeqIO.parse(handle,"fasta"):
		seqid,seq = str(s.id), str(s.seq)
		try:
			homologids = homoDICT[seqid]
			for homologid in homologids:
				with open(outdir+"/"+homologid+".fa","a") as outfile:
					outfile.write(">"+seqid+"\n"+seq+"\n")
		except: outfile1.write(seqid+"\n")
	outfile1.close()
	handle.close()
	

	
