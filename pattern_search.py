"""
Each line of the taxon table looks like:
taxonID	Family_Genera_speices
"""

# filteres for cary clade-wise searches
MIN_CARY_TIP = 30
MIN_CARY_FAMILY = 15
MAX_CORE_ANTHO = 1

import sys,os
import newick3,phylo3

# 9 in total. technically not all are non-core but leave microteaceae here
non_core = ["Nepenthaceae","Drosophyllaceae","Droseraceae","Physenaceae","Polygonaceae",\
			"Plumbaginaceae","Simmondsiaceae","Frankeniaceae","Tamaricaceae","Microteaceae"]

# 16 in total
core = ["Achatocarpaceae","Aizoaceae","Amaranthaceae","Anacampserotaceae","Basellaceae",\
		"Cactaceae","Caryophyllaceae","Chenopodiaceae","Molluginaceae","Montiaceae",\
		"Nyctaginaceae","Phytolaccaceae","Portulacaceae","Rivinaceae","Sarcobataceae",\
		"Talinaceae"]
		
def get_front_labels(node):
	"""given a node, return a list of front tip labels"""
	leaves = node.leaves()
	return [i.label for i in leaves]

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python pattern_search.py table dir_ingroup_clades outdir"
		sys.exit(0)
	
	table,indir,outdir = sys.argv[1:]
	if indir[-1] != "/": indir += "/"
	if outdir[-1] != "/": outdir += "/"
	
	familyDICT = {} #key is seq acronym, value is family
	speciesDICT = {} #key is seq acronym, value is full taxon name
	with open(table, "rU") as infile:
		for line in infile:
			spls = line.strip().split("\t")
			if len(spls) < 2: continue
			speciesDICT[spls[0]] = spls[1]
			for i in spls[1].split("_"):	
				if i in core:
					familyDICT[spls[0]] = i
	
	count = 0 # keep track of number of trees read
	for i in os.listdir(indir):
		count += 1
		with open(indir+i,"r") as infile:
			tree = newick3.parse(infile.readline())
		for node in tree.iternodes():
			# get all the families that belong to the core Caryophyllales
			# if front contain any non-Cary taxa, ignore the node
			labels = get_front_labels(node)
			if len(labels) < MIN_CARY_TIP: continue
			cary_families = [] # all the cary families in front
			all_cary_in_front = True
			for label in labels: # remove color labels
				print label
				try:
					cary_families.append(familyDICT[label.split("@")[0]])
				except:
					all_cary_in_front = False
					break
			print labels
			print cary_families
			print
			
			if not all_cary_in_front: continue
			if "Amaranthaceae" not in cary_families:
				continue # require that Amaranthaceae present			
			# look for absolute number of antho vs. all
			num_cary_family = len(set(cary_families))
			num_cary_tip = len(cary_families)
			num_core_antho = 0 # num of tips that belong to Mollugonaceae or Caryophyllaceae
			for family in cary_families:
				if family == "Molluginaceae" or family == "Caryophyllaceae":
						num_core_antho += 1
			pass_filter = False
			if num_cary_tip >= MIN_CARY_TIP and num_cary_family >= MIN_CARY_FAMILY and num_core_antho <= MAX_CORE_ANTHO:
				print i, num_cary_tip, num_cary_family, num_core_antho
				print set(cary_families)
				pass_filter = True
				break
		if pass_filter:
			for node in tree.iternodes():
				if node.istip:
					label = node.label
					taxonid, seqid = label.split("@")
					node.label = speciesDICT[taxonid]+"@"+seqid
			with open(outdir+i+".name","w") as outfile:
				outfile.write(newick3.tostring(tree)+";\n")
