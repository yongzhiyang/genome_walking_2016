"""
Remove bait peptides that are less than 30 aa in lengths
sort splice variants into one bait file
"""

import sys,os
import seq
import networkx as nx

def get_geneid(seqid):
	"""seq looks like Beta@Bv2_029870_niqq_t1
	gene id should be Bv2_029870_niqq"""
	print seqid
	spls = (seqid.split("@")[1]).split("_")
	assert len(spls) == 4, "check seqid format"
	return spls[0]+"_"+spls[1]+"_"+spls[2]

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python spls_baits.py fasta outdir"
		sys.exit(0)
	
	fasta,outdir = sys.argv[1:]
	
	for s in seq.read_fasta_file(fasta):
		seqid,seq = s.name,s.seq
		if len(seq) < 30:
			continue
		geneid = get_geneid(seqid)
		with open(outdir+"/"+geneid,"a") as outfile:
			outfile.write(">"+seqid+"\n"+seq+"\n")

	



