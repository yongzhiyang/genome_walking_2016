from Bio import SeqIO
import sys,os


if __name__ =="__main__":
	if len(sys.argv) != 3:
		print "usage: python process_phytozome_seqIDs.py inDIR outDIR"
		sys.exit()
	
	inDIR = sys.argv[1]+"/"
	outDIR = sys.argv[2]+"/"
	
	for i in os.listdir(inDIR):
		taxonID = i.split("_")[0]
		if "protein" in i:
			outname = taxonID+".pep.fa"
		elif "cds" in i:
			outname = taxonID+".cds.fa"
		else:
			print i,"check file names"
			sys.exit()
		print i,outname
		infile = open(inDIR+i,"r")
		outfile = open(outDIR+outname,"w")
		for line in infile:
			if line[0] == ">": #seqID
				seqID = line.split(" ")[0]
				seqID = seqID.split("|")[0]
				seqID = seqID.replace(".","_")
				seqID = seqID.replace(">",">"+taxonID+"@")
				outfile.write(seqID+"\n")
			else:
				outfile.write(line)
		infile.close()
		outfile.close()
