This is a temperary repository for homolog tree construction by genome walking from our 218 taxa Caryophyllales data set

File format setups are the same as https://bitbucket.org/yangya/phylogenomic_dataset_construction: choose a taxonID for each data set. This taxonID will be used throughout the analysis. Use short taxonIDs with 4-6 letters and digits with no special characters. Avoid special characters except "_" for seq ids. 

Disclaimer: file formats are hard coded and code may not give the most informative error message for formatting issues. See Dryad repository XXX for actual files used. 

###Prepare backbone homologs
Concatenate all 43 genome peptide sequences into the fasta file 43genomes.pep.fa. Make a blast database using this combined peptide fasta file:

	makeblastdb -in 43genomes.pep.fa -parse_seqids -dbtype prot -out 43genomes.pep.fa

For each beet locus, combine the isoforms into one single fasta file.

	python spls_baits.py Beta.pep.fa <outdir>

To build a "phylome" like homolog tree for each beet locus, the following pipeline was used to do the SWIPE search, parse and filtering results, and build and trim the putative homolog tree.

	python bait_homologs_genome.py <query_pep_fa_dir> 43genomes.pep.fa num_cores outdir

This creats refined homolog trees for each beet locus that end with .subtree, which are the phylomes. Copy these refined homolog trees into a new dir and merge homologs according to these refined trees

	python merge_genome_clusters.py <phylome dir>

This output a file named "beet_clusters_merged", with each line having beet loci that were duplicated within the Caryophyllales and should be merged into a single homolog. 


###Sort transcriptome data according to the backbone homologs
Run cd-hit on each peptide data set translated from assembled transcriptomes
For example, for the peptide data set AAXJ.pep.fa

	cd-hit -i AAXJ.pep.fa -o AAXJ.cdhit.pep.fa -c 0.99 -n 5 -T 4

SWIPE each peptide sequence from the transcriptomes against the beet proteome with an E value cutoff of 0.01. The top hit for each query peptide was recorded in a dir called "swipe_output". Also, concatenate all peptide files from genomes and transcriptomes into 218taxa.pep.fa

	python sort_homologs_genome_walking.py <dir containing *.subtree> beet_clusters_merged swipe_output_dir <path to 218taxa.pep.fa> outdir

This will output homologs with transcripts added, and a file named beet_clusters_merged. I moved all homologs larger than 5000 to a separate dir and ignore them for now. 

###Refine homologs
A tree was constructed for each resulting fasta file with the script "fasta_to_tree.py", and 

	python trim_tips.py <tree dir> .tre 1 2
	python mask_tips_by_taxonID_transcripts.py . '/media/data/YA/bait/genome_walking/5_homologs/aln-cln' y
	python refine_homolog_genome_walking.py <combined pep fasta file for all taxa> beet_clusters_merged <input dir> <output dir> <branch length cutoff>

This process is repeated once or twice.

Final trimming settings (more stringent):

	python trim_tips.py <tree dir> .tre 0.5 1
	python ../../../wgd_2016/cut_long_internal_branches.py . .tt 0.5 4 '/media/yayang/data/YA/bait/analyses_backup/2015_208_taxa/genome_walking/6_refine/6_mm_4-4LR-5' 
 

###Orthology inference and building species trees
Scripts in this step can be found in bitbucket.org/yangya/phylogenomic_dataset_construction
	
	python prune_paralogs_RT.py <homoTreeDIR> <homotree_file_eneding> <outDIR> <min_ingroup_taxa> taxon_code_file

python ~/clustering/write_ortholog_fasta_files.py '/media/yayang/data/YA/bait/analyses_backup/2016_218taxa_WGD_gypsum/data/218taxa_pep.fa' '/media/yayang/data/YA/bait/analyses_backup/2016_218taxa_WGD_gypsum/genome_walking/7_ortho_filter160/ortho_tre' '/media/yayang/data/YA/bait/analyses_backup/2016_218taxa_WGD_gypsum/genome_walking/7_ortho_filter160/fasta_pep' 160

python ~/clustering/write_ortholog_fasta_files.py '/media/yayang/data/YA/bait/analyses_backup/2016_218taxa_WGD_gypsum/data/218taxa_cds.fa' '/media/yayang/data/YA/bait/analyses_backup/2016_218taxa_WGD_gypsum/genome_walking/7_ortho_filter160/ortho_tre' '/media/yayang/data/YA/bait/analyses_backup/2016_218taxa_WGD_gypsum/genome_walking/7_ortho_filter160/fasta_cds' 160

python ../../../../wgd_2016/prank_wrapper.py  . . .fa aa
python ../../../wgd_2016/phyutility_wrapper.py . .3 aa


trim tip by 0.3, 0.6

	python ~/genome_walking_src_2016/trim_tips.py . .tre 0.3 0.6

Remove internal branches longer than 0.5

	python ~/genome_walking_src_2016/cut_long_internal_branches.py . .tt 0.5 160 .


Write alignment from trimmed tree, phyutility by 0.3, and bootstrap

	python ~/genome_walking_2016/raxml_bs_wrapper.py . 4 aa





Concatenate all the trimmed trees

	cat *tt >all_ML_trees

Estimate MQSST tree using ASTRAL with minimal number of taxa set to 160, calculate quatet support, 

	java -jar astral.4.10.2.jar -i all_ML_trees -o filter160_astral.tre -m 160

Fix output tree format so that figtree can read it

	python remove_output_tree_labels.py filter160_astral.tre 

Write fasta files from trees

cut with 0.3 

Concatenate

	#raxml -T 9 -f a -x 12345 -# 200 -p 12345 -m PROTCATAUTO -q filter150-160.model -s filter150-160.phy -n filter150-160
	raxml -T 9 -p 12345 -m PROTCATAUTO -q filter150-160.model -s filter150-160.phy -n filter150-160



